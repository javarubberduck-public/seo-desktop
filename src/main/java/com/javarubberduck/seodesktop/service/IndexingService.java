package com.javarubberduck.seodesktop.service;

import java.util.List;

public interface IndexingService {
    void indexUrls(List<String> urlsToIndex);
}
