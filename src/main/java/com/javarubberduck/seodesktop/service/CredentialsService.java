package com.javarubberduck.seodesktop.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public interface CredentialsService {
    void setCredentialsFile(String credentialsContent);
    InputStream getCredentialsInputStream();
}
