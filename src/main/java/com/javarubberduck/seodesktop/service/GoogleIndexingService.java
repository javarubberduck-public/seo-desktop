package com.javarubberduck.seodesktop.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.indexing.v3.Indexing;
import com.google.api.services.indexing.v3.model.UrlNotification;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GoogleIndexingService implements IndexingService {

    private UrlNotificationBatchListener listener;
    private CredentialsService credentialsService;

    public GoogleIndexingService(UrlNotificationBatchListener listener,
                                 CredentialsService credentialsService) {
        this.listener = listener;
        this.credentialsService = credentialsService;
    }

    @Override
    public void indexUrls(List<String> urlsToIndex) {
        Indexing indexingClient = createIndexingClient();
        BatchRequest batchRequest = indexingClient.batch();
        extractUrlsAndCreateUrlNotifications(urlsToIndex).forEach(urlNotificationRequest -> {
            try {
                indexingClient.urlNotifications().publish(urlNotificationRequest).queue(batchRequest, listener);
            } catch (IOException e) {
                // implement proper error handling
                throw new RuntimeException(e);
            }
        });

        try {
            batchRequest.execute();
        } catch (IOException e) {
            // implement proper error handling
            throw new RuntimeException(e);
        }
    }


    private List<UrlNotification> extractUrlsAndCreateUrlNotifications(List<String> urlsToIndex) {
        return urlsToIndex.stream()
                .map(url -> new UrlNotification().setUrl(url).setType("URL_UPDATED"))
                .collect(Collectors.toList());
    }

    private Indexing createIndexingClient() {
        String scopes = "https://www.googleapis.com/auth/indexing";
        JsonFactory jsonFactory = new GsonFactory();
        try {
            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            GoogleCredential googleCredential = GoogleCredential
                    .fromStream(readCredentialFiles(), httpTransport, jsonFactory)
                    .createScoped(Collections.singleton(scopes));
            return new Indexing(httpTransport, jsonFactory, googleCredential);
        } catch (GeneralSecurityException | IOException e) {
            // Provide proper error handling
            throw new RuntimeException(e);
        }
    }

    private InputStream readCredentialFiles() {
        InputStream credentialsInputStream = credentialsService.getCredentialsInputStream();
        if (credentialsInputStream == null) {
            // Provide proper error handling
            throw new RuntimeException("Could not find credentials");
        } else {
            return credentialsInputStream;
        }
    }
}
