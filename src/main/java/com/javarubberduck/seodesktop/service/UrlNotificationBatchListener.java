package com.javarubberduck.seodesktop.service;

import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.services.indexing.v3.model.PublishUrlNotificationResponse;
import com.javarubberduck.seodesktop.model.UrlIndexingResult;
import io.reactivex.rxjava3.subjects.Subject;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class UrlNotificationBatchListener extends JsonBatchCallback<PublishUrlNotificationResponse> {

    private final Subject<UrlIndexingResult> urlIndexingResultsSubject;

    public UrlNotificationBatchListener(Subject<UrlIndexingResult> urlIndexingResultsSubject) {
        this.urlIndexingResultsSubject = urlIndexingResultsSubject;
    }
    public void onSuccess(PublishUrlNotificationResponse res, HttpHeaders responseHeaders) {
        try {
            System.out.println(res.toPrettyString());
            this.urlIndexingResultsSubject.onNext(
                new UrlIndexingResult(
                        res.getUrlNotificationMetadata().getLatestUpdate().getUrl(),
                        res.getUrlNotificationMetadata().getLatestUpdate().getType()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
        System.out.println("Error Message: " + e.getMessage());
    }
}
