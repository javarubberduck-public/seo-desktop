package com.javarubberduck.seodesktop.service;

import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Service
public class CredentialsFileService implements CredentialsService {

    private String credentialsContent;

    public void setCredentialsFile(String credentialsContent) {
        this.credentialsContent = credentialsContent;
    }

    public InputStream getCredentialsInputStream() {
        return credentialsContent == null ? null : new ByteArrayInputStream(credentialsContent.getBytes());
    }
}
