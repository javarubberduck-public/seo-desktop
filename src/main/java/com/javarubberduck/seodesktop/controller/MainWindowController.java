package com.javarubberduck.seodesktop.controller;

import com.javarubberduck.seodesktop.model.UrlIndexingResult;
import com.javarubberduck.seodesktop.service.IndexingService;
import io.reactivex.rxjava3.subjects.Subject;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MainWindowController {
    @FXML
    private TextArea urlsTextInputField;
    @FXML
    private Label resultLabel;
    private final IndexingService indexingService;
    private final Subject<UrlIndexingResult> urlIndexingResultSubject;
    private final CredentialsModalController credentialsModalController;

    public MainWindowController(
            CredentialsModalController credentialsModalController,
            IndexingService indexingService,
            Subject<UrlIndexingResult> urlIndexingResultSubject) {
        this.credentialsModalController = credentialsModalController;
        this.indexingService = indexingService;
        this.urlIndexingResultSubject = urlIndexingResultSubject;
        urlIndexingResultSubject.subscribe(urlIndexingResult -> {
            resultLabel.setText("All urls were successfully submitted for indexing");
        });
    }

    @FXML
    protected void onStartIndexingButtonClick() {
        indexingService.indexUrls(List.of(urlsTextInputField.getText().split("/n")));
    }

    @FXML
    protected void onCredentialsButtonClick(ActionEvent event) {
        this.credentialsModalController.start(event);
    }

}