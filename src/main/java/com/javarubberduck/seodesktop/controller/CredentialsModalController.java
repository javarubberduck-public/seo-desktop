package com.javarubberduck.seodesktop.controller;

import com.javarubberduck.seodesktop.SeoDesktopUIApplication;
import com.javarubberduck.seodesktop.service.CredentialsService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CredentialsModalController {
    @FXML
    private TextArea credentialsInputField;
    private CredentialsService credentialsService;
    private static Stage stage;

    public CredentialsModalController(CredentialsService credentialsService) {
        this.credentialsService = credentialsService;
    }

    public void start(ActionEvent event) {
        prepareStageAndShow(event);
    }

    private void prepareStageAndShow(ActionEvent event) {
        stage = new Stage();
        Parent root = null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(SeoDesktopUIApplication.class.getResource("credentials-modal.fxml"));
            fxmlLoader.setControllerFactory(c -> this);
            root = fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        stage.setScene(new Scene(root));
        stage.setTitle("Add credentials file");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((MenuItem) event.getTarget()).getParentPopup().getOwnerWindow());
        stage.setResizable(false);
        stage.show();
    }
    @FXML
    protected void onConfirmClicked() {
        credentialsService.setCredentialsFile(credentialsInputField.getText());
        stage.close();
    }

    @FXML
    protected void onCancelClicked() {
        stage.close();
    }
}
