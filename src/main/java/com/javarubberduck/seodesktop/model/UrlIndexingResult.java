package com.javarubberduck.seodesktop.model;

public record UrlIndexingResult (String url, String status){ }
