package com.javarubberduck.seodesktop;

import com.javarubberduck.seodesktop.model.UrlIndexingResult;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
@SpringBootApplication
public class SeoDesktopSpringBootApplication {
    public static void main(String[] args) {
        Application.launch(SeoDesktopUIApplication.class, args);
    }

    @Bean
    public Subject<UrlIndexingResult> urlIndexingResultsSubject(){
        return PublishSubject.create();
    }

}
