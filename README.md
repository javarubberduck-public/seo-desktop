# seo-desktop
[google api github repo](https://googleapis.github.io/google-api-java-client/)
[google indexing example](https://developers.google.com/search/apis/indexing-api/v3/prereqs#java)
[batching example](https://googleapis.github.io/google-api-java-client/batching.html)

# Command to build native application

```jpackage --input build --main-jar ./libs/seo-desktop-1.0-SNAPSHOT.jar -t dmg --name "SEO desktop app"```